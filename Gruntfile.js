"use strict";

module.exports = function(grunt) {

	grunt.initConfig({
		ngconstant: {
      options: {
        space: '  ',
        name: 'config',
        dest: '.tmp/scripts/config.js',
      },
      // Environment targets
      local: {
        constants: {
          env: {
            name: 'testing',
            apiUrl: 'http://localhost:8080'
          }
        }
      },
      development: {
        constants: {
          env: {
            name: 'staging',
            apiUrl: 'https://ec2-52-51-139-20.eu-west-1.compute.amazonaws.com'//'http://localhost:8080'
          }
        }
      },
      production: {
        constants: {
          env: {
            name: 'production',
            apiUrl: 'https://ec2-52-18-99-146.eu-west-1.compute.amazonaws.com'
          }
        }
      }
    },
		jshint: {
			files: ['Gruntfile.js', 'js/**/*.js', 'js/*.js'],
			options: {
		        globalstrict: true,
		        globals : {
		        	angular : true,
		        	app : true,
		        	module : true,
		        	sessionStorage : true,
		        	$ : true,
		        	$sce:true,
		        	console : true,
		        	FormData : true,
		        	document : true,
		        	window : true
		        }
		    },
		},

		less: {
			style: {
				files: {
					"css/base.css": "less/app.less"
				}
			}
		},

		watch: {
			options: {
		      livereload: true,
		    },
			css: {
				files: ['less/*.less', 'css/*.css'],
				tasks: ['less']
			},
			scripts: {
			    files: ['js/*.js', 'js/**/*.js'],
			    tasks: ['default']
			 }
		}

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-ng-constant');
	// grunt.loadNpmTasks('grunt-contrib-jshint');


	grunt.registerTask('default', [
		'less',
	]);

	grunt.registerTask('buildProduction', [
		'ngconstant:production'
	])

	grunt.registerTask('buildLocal', [
		'ngconstant:local'
	])

	grunt.task.run([
    'ngconstant:development'
  ]);

};
