# ANGULAR 1 Client App

Consumes Vizbot-Back, an express-node-mongo API server

Hosted on Heroku

- production: vizbot-app
- staging: vizbot-staging

## Building different environments
local testing: `grunt buildLocal`  
production: `grunt buildProduction`  
staging: `grunt`

these grunt tasks create the environment config file, which sets the url to which the API calls are sent
they also compile the SASS into CSS

## Testing

End-to-end using Jasmine and [Protractor](http://www.protractortest.org/#/)  
Make sure you've got Java installed to run the webdriver that manages the selenium server.  
`npm run t-setup-0` in one tab [if required]  
`npm run t-setup-1` in another tab   
`npm run t-setup-2` in another tab  
`npm test` in another tab  




