"use strict";

 app.factory('User', ['$resource', 'env', function($resource, env) {
  var url = env.apiUrl
	return $resource(url + '/users/:id', {id : '@id'}, {
		all: { //actions
			method: "GET",isArray: true, url: url + "/users", headers:{
				'Authorization': 'Basic dml6Ym90QHZpemJvdC5jb206dml6Ym90'
			}
		},
		byConsent: {
			method: "GET", isArray: true, url: url + "/consents/people", headers:{
				'Authorization': 'Basic dml6Ym90QHZpemJvdC5jb206dml6Ym90'
			}
		},
		logIn: { ///http://ec2-52-18-99-146.eu-west-1.compute.amazonaws.com/users
			method:"PUT", url: url +"/users", headers:{
				'Authorization': 'Basic dml6Ym90QHZpemJvdC5jb206dml6Ym90'
			}
		}
	});
}]);

 app.factory('Consent', ['$resource', 'env', function($resource, env) {
  var url = env.apiUrl
	return $resource(url + '/consents/:_id', {_id: '@_id'}, {
    get: {
      method: "GET",
      headers: {
        'Authorization': 'Basic dml6Ym90QHZpemJvdC5jb206dml6Ym90'
      }
    },
    all: {
      method: "GET"
    },
    byUser: {
      method: "GET",
      isArray: true,
      url: url + "/users/:userId/consents",
      headers: {
        'Authorization': 'Basic dml6Ym90QHZpemJvdC5jb206dml6Ym90'
      }
    },
    getDocuments: {
      method: "GET",
      isArray: true,
      url: url + "/consents/:_id/consentDocuments",
      headers: {
        'Authorization': 'Basic dml6Ym90QHZpemJvdC5jb206dml6Ym90'
      }
    }
  });
}]);
