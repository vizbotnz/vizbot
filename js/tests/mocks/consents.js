module.exports = [
  { 
    request: {
      path: '/consents',
      method: 'POST',
    },
    response: {
      status: 200,
      id: '1123123asdas123123'
    }
  },
  {
    request: {
      path: '/consents/1123123asdas123123',
      method: 'GET',
    },
    response: {
      status: 304
    }
  },
  {
    request: {
      path: '/consents/1123123asdas123123',
      method: 'POST',
    },
    response: {
      status: 200
    }
  }
]