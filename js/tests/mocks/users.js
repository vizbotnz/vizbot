module.exports = [
  {
    request: {
      path: '/users',
      method: 'PUT',
      // params: {
      //   mail: "email@email.com",
      //   pwd: "totallyUnprotected"
      // }
    },
    response: {
      headers: {
        id: '572bbfee16ad9c6762ddef5a'
      },
      status: 200
    },
  },
  {
    request: {
      path: '/users/572bbfee16ad9c6762ddef5a',
      method: 'GET'
    },
    response: {
      data: {
        _id: "572bbfee16ad9c6762ddef5a",
        pwd: "$2a$05$N59jgkZgO3vlTNjHYY1z/.IEcxBj7ZcS5yXIuMH3Cm7bOLOJjnoc2",
        mail: "gmyoung321@gmail.com",
        name: "petergabriel",
        __v: 6,
        team: [],
        shareConsents: [],
        consents: [
          "572bc04016ad9c6762ddef5c",
          "5743950b16ad9c6762ddef99",
          "57467a7c16ad9c6762ddefdf",
          "574cb7fe16ad9c6762ddf01b",
          "574cbaaa16ad9c6762ddf01e",
          "574cc81016ad9c6762ddf021"
        ],
        address: {
          street: "36 Hathaway Avenue",
          city: "Lower Hutt",
          postCode: "5010"
        }
      },
      status: 200
    }
  },
  {
    request: {
      path: '/users/572bbfee16ad9c6762ddef5a/consents',
      method: 'GET',
      // data: {
      //   mail: "email@email.com",
      //   pwd: "totallyUnprotected"
      // }
    },
    response: {
      data: {
        _id: "574cbaaa16ad9c6762ddf01e",
        buildingWork: "asdfasdfasdf",
        old: 1234,
        numberPeople: 23,
        status: "create",
        address: "w",
        role: "Agent",
        title: "asdfasdfsadf",
        user: "572bbfee16ad9c6762ddef5a",
        __v: 0,
        submitted: false,
        RFI: [],
        notifications: [],
        compliance: [],
        doc: [],
        people: [],
        lbp: [],
        contact: {
          name: "petergabriel",
          mail: "gmyoung321@gmail.com"
        },
        agent: {
          name: "petergabriel",
          mail: "gmyoung321@gmail.com"
        },
        project: {
          info: false,
          value: "2342343"
        },
        buildingInfo: {
          name: "thenameishere",
          level: "three",
          totalFloor: "2342",
          area: "234234",
          speSystem: false,
          address: "w"
        },
        processing: [],
        newUse: null,
        lawfullyUse: {
          first: "housing",
          second: "detached"
        }
      },
      status: 200
    }
  }
]
