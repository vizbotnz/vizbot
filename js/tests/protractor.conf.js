
var DisplayProcessor = require('./../../node_modules/jasmine-spec-reporter/src/display-processor');

function TimeProcessor(options) {
}

function getTime() {
    var now = new Date();
    return now.getHours() + ':' +
           now.getMinutes() + ':' +
           now.getSeconds()
}

TimeProcessor.prototype = new DisplayProcessor();

TimeProcessor.prototype.displaySuite = function (suite, log) {
  return getTime() + ' - ' + log ;
};

TimeProcessor.prototype.displaySuccessfulSpec = function (spec, log) {
  return getTime() + ' - ' + log;
};

TimeProcessor.prototype.displayFailedSpec = function (spec, log) {
  console.log(spec);
  return getTime() + ' - ' + log;
};

TimeProcessor.prototype.displayPendingSpec = function (spec, log) {
  return getTime() + ' - ' + log;
};


exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub', // we have this fake browser. does it matter that that is what is getting clicked around on?
  capabilities: {
    'browserName': 'chrome'
  },
  framework: 'jasmine',
  directConnect: true,
  baseUrl: 'http://localhost:9000',
  specs: [
    'spec/*.spec.js'
  ],
  jasmineNodeOpts: {
    defaultTimeoutInterval: 50000,
    print: function () {}
  },
  mocks: {
    dir: 'mocks',
    default: []//['users']
  },
  onPrepare: function(){
    var SpecReporter = require('jasmine-spec-reporter');
    jasmine.getEnv().addReporter(new SpecReporter({
      displayStacktrace: 'none',    // display stacktrace for each failed assertion, values: (all|specs|summary|none)
      displayFailuresSummary: true, // display summary of all failures after execution
      displayPendingSummary: true,  // display summary of all pending specs after execution
      displaySuccessfulSpec: true,  // display each successful spec
      displayFailedSpec: false,      // display each failed spec
      displayPendingSpec: false,    // display each pending spec
      displaySpecDuration: false,   // display each spec duration
      displaySuiteNumber: true,    // display each suite number (hierarchical)
      colors: {
        success: 'green',
        failure: 'red',
        pending: 'yellow'
      },
      prefixes: {
        success: '✓ ',
        failure: '✗ ',
        pending: '* '
      },
      customProcessors: [TimeProcessor]
    }));
    require('protractor-http-mock').config = {
        rootDirectory: __dirname,//'/Users/Gabrielle/Documents/contracts/vizbot/test' // default value: process.cwd()
        protractorConfig: 'protractor.conf.js' // default value: 'protractor-conf.js'
    };
    browser.manage().window().setSize(1200, 1000);
    global.EC = protractor.ExpectedConditions  // to handle modal box appearance delay
  }
}
