var request = require('request');
var path = require('path')
var traverse = require('traverse');
var getIn = require('get-in')

var consentData = require('./../data/consent-data')
var userData = require('./../data/user-data')
consentData.id = userData.id
consentData.agent.name = userData.name
consentData.agent.phone = userData.phone
consentData.agent.mail = userData.mail
consentData.agent.address = userData.address

var apiUrl = 'http://localhost:8080' // should be driven by grunt build, not hardcoded here

var allRequiredData = flatten(consentData)
var dataEntered = []

// slowTests(10)

describe('vizbot', function () {
  it('purges the test database, test upload files and test fdf pdf creation files', function () {
    request.delete(apiUrl + '/testDbPurge', {}, function (err, res) {
      if (err) throw err
      console.log('DB Purged');
    })
  })
  it('registers and makes consent', function () {
    browser.get('');
    expect(browser.getTitle()).toEqual('Home');
    registerUser()
    completeConsent()
  })
  it('checks doctype checkboxes (sometimes misses some!)', function () {
    browser.get('');
    expect(browser.getTitle()).toEqual('Home');
    fillModelField('login.mail', userData.mail)
    fillModelField('login.pwd', userData.pwd)
    clickButton('Log in')
    clickButton('View')
    clickLink('Application Information')
    // browser.wait(EC.visibilityOf(element(by.css(".docTypesCheckbox"))), 50000)
    element.all(by.css(".docTypeCheckbox")).then(function (checkboxes) {
      checkboxes.forEach(function (checkbox) {
        checkbox.click()
      })
    })
  })
  it('makes completed consent', function () {
    browser.get('')
    fillModelField('login.mail', userData.mail)
    fillModelField('login.pwd', userData.pwd)
    clickButton('Log in')
    element(by.buttonText('View')).evaluate('consent').then(function (consent) {
      assertExpectedIsSubset(it, expect, consent, consentData)
    })
  })
  it('signs in and downloads consent application form and supporting docs', function () {
    browser.get('')
    fillModelField('login.mail', userData.mail)
    fillModelField('login.pwd', userData.pwd)
    clickButton('Log in')
    clickButton('View')
    clickLink('Application Information')
    clickLink('Download consent application and supporting documents')
    p() // uncomment to check generated PDF and docs downloaded
  })
})


function assertExpectedIsSubset (it, assert, actual, expected) {
  traverse(expected).forEach(function (expectedNode) {
    var actualNode = getIn(actual, this.path)
    if (this.isLeaf
      && !this.path.toString().match(/id|user|submitted/)
      && !expectedNode.toString().match(/Alternative|Other/)
      && !(this.path[0].toString() === 'doc')) {
      assert(actualNode).toEqual(expectedNode, '\n                                         ' + this.path.join('.'))//, this.path + ' should be equal')
    }
  })
}

function registerUser () {
    clickButton('Sign up')
    fillModelField('register.name', userData.name)
    fillModelField('register.mail', userData.mail)
    fillModelField('register.pwd', userData.pwd)
    fillModelField('register.address.street', userData.address.street)
    fillModelField('register.address.city', userData.address.city)
    fillModelField('register.address.postCode', userData.address.postCode)
    fillModelField('register.phone', userData.phone)
    clickNgClickFunc('create()')
}

function completeConsent () {
  clickButton('Create project')

  fillModelField('newconsent.title', consentData.title)
  fillModelField('newconsent.address', consentData.buildingInfo.address)
  fillModelField('newconsent.legalDescription', consentData.legalDescription)
  fillModelField('newconsent.project.value', consentData.project.value)
  clickButton('Next')

  clickNgClickFunc('step(1)')
  clickNgClickFunc('dwelling(1)')
  fillModelField('newconsent.numberOccupants', consentData.numberOccupants)
  fillModelField('newconsent.old', consentData.old)
  fillModelField('newconsent.buildingInfo.intendedLife', consentData.buildingInfo.intendedLife)
  clickButton('Continue')
  fillModelField('newconsent.buildingWork', consentData.buildingWork)
  clickById('inlineRadio3')
  clickNgClickFunc('optionYes()')
  clickNgClickFunc('step(42)')
  clickNgClickFunc('community(42)')
  clickButton('Save')

  clickButton('View')
  clickLink('Compile Application')

  clickButton('Edit')//should be = clickButton('Building & Project Information')
  fillModelField('consent.buildingInfo.name', consentData.buildingInfo.name)
  fillModelField('consent.buildingInfo.level', consentData.buildingInfo.level)
  fillModelField('consent.buildingInfo.totalFloor', consentData.buildingInfo.totalFloor)
  fillModelField('consent.buildingInfo.existingFloorArea', consentData.buildingInfo.existingFloorArea)
  fillModelField('consent.buildingInfo.proposedFloorArea', consentData.buildingInfo.proposedFloorArea)
  fillModelField('consent.buildingInfo.affectedLevels', consentData.buildingInfo.affectedLevels)
  fillModelField('consent.buildingInfo.locationWithinSite', consentData.buildingInfo.locationWithinSite)
  fillModelField('consent.buildingInfo.previouslyIssuedConsents', consentData.buildingInfo.previouslyIssuedConsents)
  clickById('cultural-significance-true')
  clickById("building-systems-false")
  element.all(by.css(".memorandumCheckbox")).then(function (checkboxes) {
    checkboxes.forEach(function (checkbox) {
      checkbox.click()
    })
  })
  consentData.buildingInfo.projectInformationMemorandum.forEach(function (memo, i) {
    fillModelField('consent.buildingInfo.projectInformationMemorandum['+i+'].comments', memo.comments)
  })
  // p()
  clickButton('Save')
  clickButton('People')
  clickButton('Client/Owner')
  fillModelField('client.name', consentData.client.name)
  fillModelField('client.address.street', consentData.client.address.street)
  fillModelField('client.address.suburb', consentData.client.address.suburb)
  fillModelField('client.address.city', consentData.client.address.city)
  fillModelField('client.address.postCode', consentData.client.address.postCode)
  fillModelField('client.phone', consentData.client.phone)
  fillModelField('client.mail', consentData.client.mail)
  element(by.model('client.proofOfOwnership.certificateOfTitle')).click()
  clickNgClickFunc('saveClient()')

  clickButton('Add agent details')
  fillModelField('consent.agent.relationshipToOwner', consentData.agent.relationshipToOwner)
  fillModelField('consent.agent.phone', consentData.agent.phone)
  fillModelField('consent.agent.address.suburb', consentData.agent.address.suburb)

  clickButton('Save')

  clickById('pointOfContactInvoiceClient')
  clickById('pointOfContactCorrespondenceAgent')


  clickButton('Add Professional')
  addProfessional(0)

  consentData.people.slice(1).forEach(function (person, i) {
    clickButton('More Professional')
    addProfessional(i+1)
  })
  clickById('restrictedYes')
  clickButton('Add LBP')
  fillModelField('lbp.name', consentData.lbp[0].name )
  fillModelField('lbp.particularWork', consentData.lbp[0].particularWork )
  fillModelField('lbp.classL', consentData.lbp[0].classL )
  fillModelField('lbp.lbp', consentData.lbp[0].lbp )
  fillModelField('lbp.certificat', consentData.lbp[0].certificat )
  clickNgClickFunc('saveLBP()')
  clickButton('More LBP')
  fillModelField('lbp.name', consentData.lbp[1].name )
  fillModelField('lbp.particularWork', consentData.lbp[1].particularWork )
  fillModelField('lbp.classL', consentData.lbp[1].classL )
  fillModelField('lbp.lbp', consentData.lbp[1].lbp )
  fillModelField('lbp.certificat', consentData.lbp[1].certificat )
  clickNgClickFunc('saveLBP()')

  clickLink('Application information')

  clickButton('Means of Compliance')

  // var complianceCheckboxModels = ['means.b1.asnz1170', 'means.b1.b1as1', 'means.b1.NZS3604', 'means.b1.NZS4229', 'means.b1.b1vm1', 'b2.b2as1', 'b2.b2vm1', 'c.as1', 'c.as2', 'c.as3', 'c.as4', 'c.as5', 'c.as6', 'c.as7', 'c.cvm1', 'c.cvm2', 'd1.as1', 'd1.nzs4121', 'd2.as1', 'd2.as2', 'd2.as3', 'd2.nzs4121', 'e1.as1', 'e1.as3500', 'e1.vm1', 'e2.as1', 'e2.as2', 'e2.as3', 'e2.sed', 'e2.vm1', 'e3.as1', 'f1.as1', 'f1.vm1', 'f2.as1', 'f3.as1', 'f3.as1', 'f4.as1', 'f5.as1', 'f6.as1', 'f7.as1', 'f8.as1', 'f8.nzs4121', 'g1.as1', 'g1.nzs4121', 'g2.as1', 'g2.nzs4121', 'g3.as1', 'g3.nzs4121', 'g4.as1', 'g4.vm1', 'g5.as1', 'g5.vm1', 'g6.as1', 'g6.vm1', 'g7.as1', 'g7.vm1', 'g8.as1', 'g8.vm1', 'g9.as1', 'g9.vm1', 'g10.as1', 'g10.vm1', 'g11.as1', 'g11.vm1', 'g12.as1', 'g12.as2', 'g13.as1', 'g13.as2', 'g13.as3', 'g13.as3500', 'g13.vm1', 'g13.vm4', 'g14.as1', 'g14.vm1', 'g15.as1', 'h1.as1', 'h1.vm1']
  // complianceCheckboxModels.forEach(function (checkboxModel) {
  //   checkBoxClick(checkboxModel)
  // })
  element.all(by.css("input[type='checkbox']")).then(function (checkboxes) {
    checkboxes.forEach(function (checkbox) {
      checkbox.click()
    })
  })

  consentData.compliance.buildingCodeClauses.forEach(function (item, i) {
    fillModelField('consent.compliance.buildingCodeClauses['+i+'].reasonForWaiver', consentData.compliance.buildingCodeClauses[i].reasonForWaiver )
    fillModelField('consent.compliance.buildingCodeClauses['+i+'].reasonForModification', consentData.compliance.buildingCodeClauses[i].reasonForModification )
    fillModelField('consent.compliance.buildingCodeClauses['+i+'].referenceOnDrawingsAndSpecificationsAndComments', consentData.compliance.buildingCodeClauses[i].referenceOnDrawingsAndSpecificationsAndComments )
  })
  fillModelField('consent.compliance.newComplianceScheduleAddressToBeHeld', consentData.compliance.newComplianceScheduleAddressToBeHeld)
  fillModelField('consent.compliance.existingComplianceScheduleNumber', consentData.compliance.existingComplianceScheduleNumber)
  clickById('is-used-for-fire-act-things-true')
  clickButton('Save all')

  clickButton('Document')
  var fileToUpload1 = '../data/invite.ics'
  var absolutePathToFile1 = path.resolve(__dirname, fileToUpload1)
  $('input[type="file"]').sendKeys(absolutePathToFile1);
  clickButton('Upload file')

  clickLink('Application information')

  clickButton('Document')

  var fileToUpload = '../data/Beam_design_example.xlsx'
  var absolutePathToFile = path.resolve(__dirname, fileToUpload)
  $('input[type="file"]').sendKeys(absolutePathToFile);
  clickButton('Upload file')


  // clickButton('Add From ProductSpec')
  // clickLink('Cladding')
  // clickLink('Rockcote EPS Insulated Facade System')
  // clickNgClickFunc('addProduct(productDetails)')
  // clickLink('Roofing')
  // clickLink('A08 - ST963')
  // clickNgClickFunc('addProduct(productDetails)')
  // clickButton('Add to consent')

}

function addProfessional (number) {
  selectListOption(consentData.people[number].type)
  fillModelField('people.name', consentData.people[number].name)
  fillModelField('people.address.street', consentData.people[number].address.street)
  fillModelField('people.address.suburb', consentData.people[number].address.suburb)
  fillModelField('people.address.city', consentData.people[number].address.city)
  fillModelField('people.address.postCode', consentData.people[number].address.postCode)
  fillModelField('people.phone', consentData.people[number].phone)
  fillModelField('people.mail', consentData.people[number].mail)
  fillModelField('people.website', consentData.people[number].website)
  fillModelField('people.registrationNumber', consentData.people[number].registrationNumber)
  clickNgClickFunc('savePro()')
}

function checkBoxClick (ngModel) {
  element(by.model(ngModel)).click()
}

function selectListOption (optionText) {
  var listOption = element(by.cssContainingText('option', optionText))
  // browser.wait(EC.visibilityOf(listOption), 10000)
  listOption.click()
}

function clickById(id) {
  var idElement = element(by.id(id))
  // browser.wait(EC.visibilityOf(idElement), 10000)
  idElement.click()
}

function clickNgClickFunc (functionCall) {
  var clickElement = element(by.css('[ng-click="' + functionCall + '"]'))
  browser.wait(EC.visibilityOf(clickElement), 10000)
  clickElement.click()
}

function fillModelField (ngModel, fieldData) {
  dataEntered.push(fieldData)
  var field = element(by.model(ngModel))
  // browser.wait(EC.visibilityOf(field), 10000)
  field.sendKeys(fieldData)
}

function clickButton (buttonText) {
  var button = element(by.buttonText(buttonText))
  // browser.wait(EC.visibilityOf(button), 13000)
  button.click()
}

function clickLink (linkText) {
  var link = element(by.linkText(linkText))
  // browser.wait(EC.visibilityOf(link), 10000)
  link.click()
}

function flatten (nestedObject) {
  var flat = []
  Object.keys(nestedObject).forEach(function (key) {
    if (typeof(nestedObject[key]) === 'object') {
      flat.concat(flatten(nestedObject[key]))
    } else {
      flat.push(nestedObject[key])
    }
  })
  return flat
}

function slowTests (delay) {
  var origFn = browser.driver.controlFlow().execute;

  browser.driver.controlFlow().execute = function() {
    var args = arguments;

    // queue 100ms wait
    origFn.call(browser.driver.controlFlow(), function() {
      return protractor.promise.delayed(delay);
    });

    return origFn.apply(browser.driver.controlFlow(), args);
  };
}

function p () {
  browser.pause()
}
