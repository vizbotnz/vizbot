module.exports = {
  id: undefined,
  buildingWork: "We wish to change the living room into an office, with the office space suspended on pulleys from the ceiling. There will also be a pole and slide.",
  old: 2000,
  numberOccupants: 16,
  status: "create",
  address: "13 Garrett Street",
  role: "Agent",
  title: "Jacboson and Young's HQ",
  user: undefined,
  submitted: false,
  RFI: [],
  notifications: [],
  legalDescription : "Lot 4 DP 1789",
  restrictedBuildingWork: 'true',
  doc: [],
  docTypes: [
    'Plans and specifications',
    'Project information memorandum',
    'Development contribution notice',
    'Certificate attached to project information memorandum',
    'Disposal of stormwater and wastewater',
    'Building work over any existing drains or sewers or in close proximity to wells or water mains',
    'Licensed building practitioner(s) who carried out or supervised any design work that is restricted',
    'Calculations',
    'Producer Statements',
    'LBP memorandums',
  ],
  pointOfContact: {
    invoicing: "client",
    correspondence: "agent"
  },
  people: [
    {
      type: "Architect",
      name: "Peter Archi",
      phone: "0289456890",
      mail: "moonshine@gmail.com",
      website: "www.mymoonshines.com",
      registrationNumber: "12907521875",
      address: {
        street: "4 Moonshine Road",
        suburb: "Porirua",
        city: "Wellington",
        postCode: "5689"
      }
    },
    {
      type: "Structural Engineer",
      name: "Gabrielle Eng",
      phone: "0289456890",
      mail: "moo22@gmail.com",
      website: "www.mymoonshines.com",
      registrationNumber: "1098753287p",
      address: {
        street: "4 Moonshine Road",
        suburb: "Porirua",
        city: "Wellington",
        postCode: "5689"
      }
    },
    {
      type: "Builder",
      name: "BuildYoung",
      phone: "02812390",
      mail: "moo@gmail.com",
      website: "www.mymooshines.com",
      registrationNumber: "1098123s",
      address: {
        street: "4 Moonshine Road",
        suburb: "Porirua",
        city: "Wellington",
        postCode: "5689"
      }
    },
    {
      type: "Fire Engineer",
      name: "FireyJoe",
      phone: "0281239090",
      mail: "moo3@gmail.com",
      website: "www.mymooshines,bright.com",
      registrationNumber: "1098123s2",
      address: {
        street: "3 Moonshine Road",
        suburb: "Parirua",
        city: "Wollington",
        postCode: "5688"
      }
    }
  ],
  lbp: [
    {
      name: "Jacobson and Young Construction",
      classL: "very-trustworthy",
      lbp: "123123208", // lbpNumber
      certificat: "yes", // spelt right
      particularWork: "mezzanine"
    },
    {
      name: "Jacobson and Young Plumbers",
      classL: "not-trustworthy",
      lbp: "123122353253208", // lbpNumber
      certificat: "yes", // spelt right
      particularWork: "fountain"
    }
  ],
  client: {
    name: "petergabriel",
    phone: "89384998590",
    mail: "gpjacobson32@gmail.com",
    address: {
      street: "Cartoon",
      suburb: "blah",
      city: "fake",
      postCode: "213423"
    },
    proofOfOwnership: {
      certificateOfTitle: true,
      lease: false,
      agreementForSalePurchase: false
    }
  },
  agent: {
    name: "Agent Perry",
    phone: "982376532",
    mail: "agentp@gmail.com",
    address: {
      street: "sartoon st",
      suburb: "blah",
      city: "fake1",
      postCode: "2333"
    },
    relationshipToOwner: 'friend?'
  },
  project: {
    value: "100000",
  },
  buildingInfo: {
    name: "Riddle land",
    level: "3", // change to levels
    affectedLevels: "2, 3",
    totalFloor: "1000", // totalFloorArea
    existingFloorArea: "70",
    proposedFloorArea: "90",
    speSystem: false,
    address: "13 Garrett Street",
    locationWithinSite: "NW corner of section",
    intendedLife: '5',
    hasCulturalSignificance: true,
    previouslyIssuedConsents: '20/10/1989:benjamin bunny:109832109854||20/10/1999:peter rabbit:109832109887',
    projectInformationMemorandum: [
      {
        matter: "Subdivision",
        comments: "pretty standard"
      },
      {
        matter: "Alterations to land contours",
        comments: "big pipe"
      },
      {
        matter: "New or altered connection to public utilities",
        comments: "much needed..."
      },
      {
        matter: "New or altered locations and/or external dimensions of building(s)",
        comments: "much needed..."
      },
      {
        matter: "New or altered access for vehicles",
        comments: "much needed..."
      },
      {
        matter: "Building work over or adjacent to any public road or public place",
        comments: "much needed..."
      },
      {
        matter: "Disposal of stormwater or wastewater",
        comments: "much needed..."
      },
      {
        matter: "Building work over existing drains or sewers or in close proximity to wells or water mains",
        comments: "much needed..."
      },
      {
        matter: "Other matters known to the applicant that may require authorisation from Hutt City Council",
        comments: "much needed..."
      }
    ]
  },
  processing: [],
  newUse: {
    first: "communal",
    second: "communityCare"
  },
  lawfullyUse: {
    first: "housing",
    second: "detached"
  },
  riskGroupsAcceptableSolutions: [
    'Single household units and small multi-unit dwellings',
    'Sleeping (non-institutional)',
    'Care or detention',
    'Public assess and educational facilities',
    'Business, commercial and low level storage',
    'High level storage and other high risks',
    'Vehicle storage and parking'
  ],
  compliance: {
    fireService: {
      buildingMeetsSection21AFireServiceActCriteria: 'true',
      criteraFullyApplyToWorkProposed: [
        "Household units separated vertically from other fire cells, each with independent and direct exit to a safe place outside the building",
        "Outbuilding or ancillary building",
        "New building fully complying with compliance document for C1-6, D1,F6 and F8",
        "Internal fit-out, alteration, change of use or subdivision of a building with only minor effect on the fire safety systems."
      ]
    },
    newComplianceScheduleAddressToBeHeld: '4 Moonshine Road, Porirua, Wellington 5689',
    existingComplianceScheduleNumber: "1987423G7",
    buildingCodeClauses: [
      {
        clause: "B1",
        meansOfCompliance: [
          "B1/AS1",
          "NZS3604",
          "NZS4203",
          "NZS4229",
          "NZS4223",
          "Alternative"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "????",
        reasonForWaiver: "standard nonsensicle in this application",
        reasonForModification: ""
      },
      {
        clause: "B2",
        meansOfCompliance: [
          "B2/AS1",
          "NZS3101",
          "NZS3602",
          "NZS3604",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "C1 - 6",
        meansOfCompliance: [
          "C1/AS1",
          "C/AS1",
          "C/AS2",
          "C/AS3",
          "C/AS4",
          "C/AS5",
          "C/AS6",
          "C/AS7",
          "C/VM1",
          "C/VM2",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "D1",
        meansOfCompliance: [
          "D1/AS1",
          "NZS4121",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "D2",
        meansOfCompliance: [
          "D2/AS1",
          "D2/AS2",
          "D2/AS3",
          "NZS4332",
          "EN81",
          "EN115",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "E1",
        meansOfCompliance: [
          "E1/AS1",
          "AS/NZS3500.3",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "E2",
        meansOfCompliance: [
          "E2/AS1",
          "E2/AS2",
          "E2/AS3",
          "AS/NZ3500.3",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "E3",
        meansOfCompliance: [
          "E3/AS1",
          "Specific design",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "We know better",
        reasonForModification: ""
      },
      {
        clause: "F1",
        meansOfCompliance: [
          "F1/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F2",
        meansOfCompliance: [
          "F2/AS1",
          "NZS4223",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F3",
        meansOfCompliance: [
          "F3/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F4",
        meansOfCompliance: [
          "F4/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F5",
        meansOfCompliance: [
          "F5/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F6",
        meansOfCompliance: [
          "F6/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F7",
        meansOfCompliance: [
          "F7/AS1",
          "AS/NZS1668",
          "NZS4512",
          "NZS4515",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "F8",
        meansOfCompliance: [
          "F8/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G1",
        meansOfCompliance: [
          "G1/AS1",
          "Specific design",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G2",
        meansOfCompliance: [
          "G2/AS1",
          "AS/NZS3500.5",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G3",
        meansOfCompliance: [
          "G3/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G4",
        meansOfCompliance: [
          "G4/AS1",
          "AS1668.2",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G5",
        meansOfCompliance: [
          "G5/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G6",
        meansOfCompliance: [
          "G6/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G7",
        meansOfCompliance: [
          "G7/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G8",
        meansOfCompliance: [
          "G8/AS1",
          "NZS6703",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G9",
        meansOfCompliance: [
          "G9/AS1",
          "Certificate required pre-CCC",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G10",
        meansOfCompliance: [
          "G10/AS1",
          "NZS5261",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G11",
        meansOfCompliance: [
          "G11/AS1",
          "Certificate required pre-CCC",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "Qualified builder knows best",
        reasonForModification: ""
      },
      {
        clause: "G12",
        meansOfCompliance: [
          "G12/AS1",
          "G12/AS2",
          "AS/NZS3500.1",
          "AS/NZS3500.5",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G13",
        meansOfCompliance: [
          "G13/AS1",
          "G13/AS2",
          "G13/AS3",
          "AS/NZS3500.2",
          "AS/NZS3500.5",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: "New materials available, see tech spec for Alloy 189df87 attached to application"
      },
      {
        clause: "G14",
        meansOfCompliance: [
          "G14/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "G15",
        meansOfCompliance: [
          "G15/AS1",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      },
      {
        clause: "H1",
        meansOfCompliance: [
          "H1/AS1",
          "NZS4214",
          "NZS4218",
          "NZS4243",
          "NZS4305",
          "NZS4859",
          "AFL Design",
          "Alternative",
          "Other"
        ],
        referenceOnDrawingsAndSpecificationsAndComments: "",
        reasonForWaiver: "",
        reasonForModification: ""
      }
    ]
  }
}
