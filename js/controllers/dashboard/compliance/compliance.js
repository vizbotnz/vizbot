"use strict";


app.controller('ComplianceCtrl', ['$scope', 'Consent','User','fileUpload', 'env', function ($scope, Consent, User, fileUpload, env) {


	var idconsent = JSON.parse(sessionStorage.getItem('idConsentSelected'));
	$scope.compliance = '';
	$scope.urlUpload = env.apiUrl + "/consents/";
	$scope.username = JSON.parse(sessionStorage.getItem('username'));

	$scope.buildingCodeClauses = [ // 15. this should be an object, need to change the datastructure in the db
		{
			clause: "B1",
			meansOfCompliance: [
				"B1/AS1",
				"NZS3604",
				"NZS4203",
				"NZS4229",
				"NZS4223",
				"Alternative"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "B2",
			meansOfCompliance: [
				"B2/AS1",
				"NZS3101",
				"NZS3602",
				"NZS3604",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "C1 - 6",
			meansOfCompliance: [
				"C1/AS1",
				"C/AS1",
				"C/AS2",
				"C/AS3",
				"C/AS4",
				"C/AS5",
				"C/AS6",
				"C/AS7",
				"C/VM1",
				"C/VM2",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "D1",
			meansOfCompliance: [
				"D1/AS1",
				"NZS4121",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "D2",
			meansOfCompliance: [
				"D2/AS1",
				"D2/AS2",
				"D2/AS3",
				"NZS4332",
				"EN81",
				"EN115",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "E1",
			meansOfCompliance: [
				"E1/AS1",
				"AS/NZS3500.3",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "E2",
			meansOfCompliance: [
				"E2/AS1",
				"E2/AS2",
				"E2/AS3",
				"AS/NZ3500.3",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "E3",
			meansOfCompliance: [
				"E3/AS1",
				"Specific design",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F1",
			meansOfCompliance: [
				"F1/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F2",
			meansOfCompliance: [
				"F2/AS1",
				"NZS4223",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F3",
			meansOfCompliance: [
				"F3/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F4",
			meansOfCompliance: [
				"F4/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F5",
			meansOfCompliance: [
				"F5/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F6",
			meansOfCompliance: [
				"F6/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F7",
			meansOfCompliance: [
				"F7/AS1",
				"AS/NZS1668",
				"NZS4512",
				"NZS4515",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "F8",
			meansOfCompliance: [
				"F8/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G1",
			meansOfCompliance: [
				"G1/AS1",
				"Specific design",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G2",
			meansOfCompliance: [
				"G2/AS1",
				"AS/NZS3500.5",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G3",
			meansOfCompliance: [
				"G3/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G4",
			meansOfCompliance: [
				"G4/AS1",
				"AS1668.2",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G5",
			meansOfCompliance: [
				"G5/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G6",
			meansOfCompliance: [
				"G6/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G7",
			meansOfCompliance: [
				"G7/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G8",
			meansOfCompliance: [
				"G8/AS1",
				"NZS6703",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G9",
			meansOfCompliance: [
				"G9/AS1",
				"Certificate required pre-CCC",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G10",
			meansOfCompliance: [
				"G10/AS1",
				"NZS5261",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G11",
			meansOfCompliance: [
				"G11/AS1",
				"Certificate required pre-CCC",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G12",
			meansOfCompliance: [
				"G12/AS1",
				"G12/AS2",
				"AS/NZS3500.1",
				"AS/NZS3500.5",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G13",
			meansOfCompliance: [
				"G13/AS1",
				"G13/AS2",
				"G13/AS3",
				"AS/NZS3500.2",
				"AS/NZS3500.5",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G14",
			meansOfCompliance: [
				"G14/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "G15",
			meansOfCompliance: [
				"G15/AS1",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		},
		{
			clause: "H1",
			meansOfCompliance: [
				"H1/AS1",
				"NZS4214",
				"NZS4218",
				"NZS4243",
				"NZS4305",
				"NZS4859",
				"AFL Design",
				"Alternative",
				"Other"
			],
			referenceOnDrawingsAndSpecificationsAndComments: "",
			reasonForWaiver: "",
			reasonForModification: ""
		}
	]


	$scope.init = function(){
		Consent.get({_id: idconsent})
			.$promise.then(function(consent) {
				$scope.consent = consent;
				if ($scope.consent.compliance.buildingCodeClauses.length === 0) {
					$scope.consent.compliance.buildingCodeClauses = [ // 15. this should be an object, need to change the datastructure in the db
						{
							clause: "B1",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "B2",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "C1 - 6",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "D1",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "D2",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "E1",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "E2",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "E3",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F1",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F2",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F3",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F4",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F5",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F6",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F7",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "F8",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G1",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G2",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G3",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G4",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G5",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G6",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G7",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G8",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G9",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G10",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G11",
							meansOfCompliance: [
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G12",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G13",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G14",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "G15",
							meansOfCompliance: [
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						},
						{
							clause: "H1",
							meansOfCompliance: [
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false,
								false
							],
							referenceOnDrawingsAndSpecificationsAndComments: "",
							reasonForWaiver: "",
							reasonForModification: ""
						}
					]
			}
			});
	};
//http://stackoverflow.com/questions/29098203/how-to-add-checkbox-values-to-an-array-in-angularjs
	$scope.saveCompliance = function () {
		$scope.consent.$save()
	}

	$scope.uploadFile = function(compliance){
		var file = $scope.myFile;
		var uploadUrl = $scope.urlUpload + idconsent +'/codeCompliance/' + compliance ;
		var ret = fileUpload.uploadFileToUrl(file, uploadUrl);
		ret.success(function(){
			$scope.docAdded = true;
			$scope.checkInfoDocument = true;
			$('#addDocument').modal('hide');
			$scope.myFile = null;
			$scope.init();
		})
		.error(function(){
			return false;
		});
	};
}]);
